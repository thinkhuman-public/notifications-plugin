<?php
/**
 * Plugin Name:       TH Notifications
 * Description:       Configure webhook notification preferences
 * Plugin URI:        https://example.com
 * Version:           1.0.0
 * Author:            ThinkHuman
 * Author URI:        [https://www.thinkhuman.co/]
 * Requires at least: 3.0.0
 * Tested up to:      4.4.2
 *
 * @package th_notifs
 */

/*
 * Prevent direct access to this index file.
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

include_once('globals.php');

/**
 * Initialise the plugin on `plugins_loaded`
 *
 * Runs on every page.
 */
add_action('plugins_loaded', function () {
    require_once('Actions.php');
    require_once('Endpoints.php');
    require_once('UltimateMemberAccount.php');

    new UltimateMemberAccount();
    new Endpoints();
    new Actions();

    // Generate the API Key if one does not exist
    if(get_option('th_notifs_plugin_options') === false) {
        update_option('th_notifs_plugin_options', ['api_key' => md5(uniqid('th_notifs'))]);
    }

});

add_action('admin_init', 'th_register_settings');

add_action( 'admin_menu', 'th_add_settings_page' );
add_action( 'edit_user_profile', 'th_extra_user_profile_fields' );
add_action( 'show_user_profile', 'th_extra_user_profile_fields' );

function th_add_settings_page() {
    add_menu_page( 'Notification Settings', 'Notification Settings', 'manage_options', 'th-settings', 'th_render_plugin_settings_page' );
    add_submenu_page( 'th-settings', 'Template Settings', 'Templates', 'manage_options', 'th-template-settings', 'th_render_template_settings_page');
}


function th_extra_user_profile_fields($user) {

    $level_options = [
        'warning' => 'Warning',
        'critical' => 'Critical'
    ];
    $notification_level = get_the_author_meta('th_notification_level', $user->ID);


    ?>
    <h3><?php _e("User Notification Settings", "blank"); ?></h3>

    <table class="form-table">
        <tr>
            <th>
                <label for="address"><?php _e("Desired Notification Level"); ?></label>
            </th>
            <td>
                <select name="th_notification_level">
                    <?php foreach($level_options as $option => $label): ?>
                        <option <?php if($option === $notification_level): ?> selected="selected" <?php endif; ?> value="<?=$option;?>"><?=$label;?></option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
    </table>
<?php }

function th_render_plugin_settings_page() {
    ?>
    <h2>Notification Settings</h2>
    <form action="options.php" method="post">
        <?php
        settings_fields( 'th_notifs_plugin_options' );
        do_settings_sections( 'th_notifs_plugin' ); ?>
        <input name="submit" class="button button-primary" type="submit" value="<?php esc_attr_e( 'Save' ); ?>" />
    </form>
    <?php
}

function th_render_template_settings_page() {
    th_plugin_include('/templates/settings/templates.php');
}

function th_register_settings()
{
    register_setting('th_notifs_plugin_options', 'th_notifs_plugin_options', 'th_notifs_plugin_options_validate');
    add_settings_section('api_settings', 'Webhook Settings', 'th_plugin_section_text', 'th_notifs_plugin');

    add_settings_field('th_plugin_setting_api_key', 'API Key', 'th_plugin_setting_api_key', 'th_notifs_plugin', 'api_settings');

    add_settings_section('mail_settings', 'Mail Settings', 'th_plugin_section_text', 'th_notifs_plugin');
    add_settings_field('th_plugin_setting_sendgrid_api_key', 'Sendgrid API Key', 'th_plugin_setting_sendgrid_api_key', 'th_notifs_plugin', 'mail_settings');
}

function th_notifs_plugin_options_validate($input)
{
    $newinput['api_key'] = trim($input['api_key']);
    $newinput['sendgrid_api_key'] = trim($input['sendgrid_api_key']);
    if (!preg_match('/^[a-z0-9]{32}$/i', $newinput['api_key'])) {
        $newinput['api_key'] = '';
    }

    return $newinput;
}


function th_plugin_section_text() {
    echo '<p>Here you can set all the options for using the notifications webhook</p>';
}

function th_plugin_setting_api_key() {
    $options = get_option( 'th_notifs_plugin_options' );
    echo "<input id='th_plugin_setting_api_key' name='th_notifs_plugin_options[api_key]' type='text' value='" . esc_attr( $options['api_key'] ) . "' />";
}

function th_plugin_setting_sendgrid_api_key() {
    $options = get_option( 'th_notifs_plugin_options' );
    echo "<input id='th_notifs_plugin_sengrid_api_key' name='th_notifs_plugin_options[sendgrid_api_key]' type='text' value='" . esc_attr( $options['sendgrid_api_key'] ) . "' />";
}

function th_plugin_setting_results_limit() {
    $options = get_option( 'th_notifs_plugin_options' );
    echo "<input id='th_plugin_setting_results_limit' name='th_notifs_plugin_options[results_limit]' type='text' value='" . esc_attr( $options['results_limit'] ) . "' />";
}

function th_plugin_setting_start_date() {
    $options = get_option( 'th_notifs_plugin_options' );
    echo "<input id='th_plugin_setting_start_date' name='th_notifs_plugin_options[start_date]' type='text' value='" . esc_attr( $options['start_date'] ) . "' />";
}
