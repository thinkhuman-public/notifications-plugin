<?php

/*
 * This is an example model for when you create a custom database table.
 */
class ExampleModel
{
    public $table = 'wp_th_custom_table';

    public function create($data)
    {
        global $wpdb;
        $insert = $wpdb->insert($this->table, $data);
        if ($insert) {
            return $wpdb->insert_id;
        }
        return false;
    }

    public function update($id, $data)
    {
        global $wpdb;
        return $wpdb->update($this->table, $data, [
            'id' => $id
        ]);
    }

    public function get($id)
    {
        global $wpdb;
        return $wpdb->get_row($wpdb->prepare("
            SELECT * FROM {$this->table} WHERE id = %d
        ", $id));
    }
}