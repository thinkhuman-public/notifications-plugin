# TH Notifications Plugin

This plugin opens a public webhook that allows secondary systems to trigger notification emails and send them to
users present in the wordpress system. The plugin allows users to configure their desired level of notification interaction
via a new Notification Preferences tab.

## Installation
Drop this git repo into your /wp-content/plugins directory and then activate it in your wp-admin backend.

## Usage

### Changing your Notification Preferences
TBD 

### Viewing the API Key
For the webhook to function, you will need to send an api_key parameter along with it. To view/change this 
value navigate to your wp-admin backend and find your Settings item in the left sidebar. 
From there you will see a "Notification Settings" submenu icon. Click this to navigate to the webhook 
settings page.

## Reference
How does this plugin work?

## Quickstart Structure
Use the annotated file structure below to familiarize yourself with the important files and their uses
```text
/
- templates/
-- notification_settings.php # Defines the notification preferences form, change me to update preferences.
- Endpoints.php # Defines the webhook. Change me to update email sending functionality.
- index.php # Entrypoint for the plugin. Start here to trace end to end.

```


## Webhook
The webhook function works by exposing an unprivileged admin-ajax.php action. Unprivileged being a reference 
solely to the operaton of actions under native wordpress plugins. Authentication for this plugin is handled by an 
admin-configured (or randomly generated), api_key that must be present in each webhook request.

Configuraton for this can be found in the 'Endpoints.php' file under the `wp_ajax_nopriv_th_email_hook` hook.

## Request Details
While the readme provides a brief overview of the core webhook function; a complete specification of the webhook 
request can be found in the docs/swagger.yml file.

Sample Request:
```text
POST https://wp.test.com/wp-admin/admin-ajax.php?action=th_email_hook&api_key=123456
```
Body
```json
{ 
  "level" : "critical"
}
```
Response
```json
{
	"success": true,
	"message": "Emails Sent",
	"data": [
		{
			"user_id": "9",
			"success": true
		}
	]
}
```
Body Schema
```text
level    : Describes how severe the alert is, can be any of : ["everything", "critical"]
```