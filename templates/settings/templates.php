<?php
$templates = Endpoints::getTemplates();
?>


<h2>Template Settings</h2>

<h3>Add New Template</h3>
<form action="/wp-admin/admin-ajax.php?action=create_template" method="post">
    <label>
        Template Name
    </label>
    <br />
    <input type="text" name="template_name" autocomplete="off">
    <br />
    <label>Template Body</label>
    <br />
    <textarea name="template_body"></textarea>
    <br />
    <input name="submit" class="button button-primary" type="submit" value="<?php esc_attr_e( 'Save' ); ?>" />
</form>

<h3>Current Templates</h3>
<table>
    <thead>
    <tr>
        <th>
            Name
        </th>

        <th>
            Type Slug
        </th>
    </tr>
    </thead>
    <?php foreach($templates as $template): ?>
        <tr>
            <td><?=$template['name']?></td>
            <td><?=$template['slug']?></td>
        </tr>
    <?php endforeach; ?>
</table>