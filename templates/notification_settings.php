
<style>
    .notifications-input-container select[multiple] option:checked {
        background: #1e90ff;
        color: #fff;
    }
    .notifications-input-container select[multiple] {
        min-height: 115px;
        overflow:hidden;
        background-image: none;
    }

    .um-account-tab {
        padding: 1.5rem;background: #ececec;border-radius: 10px;
    }
</style>

<div class="notifications-input-container" style="display: flex; gap: 35px; margin-top: 1rem;">

    <div>
        <label for="th_notification_settings" style="padding-bottom: 10px;">Receive Alerts for...</label>
        <select id="th_notification_settings" name="th_notification_settings">

            <?php
            $settings_options = isset($settings_options) ? $settings_options : [];
            $specific_selected =
                isset($settings_options['only_specific']['selected'])
                && $settings_options['only_specific']['selected'] === true;
            foreach($settings_options as $option_key => $option): ?>

                <option <?php if($option['selected'] === true){echo ' selected="selected" ';}?> value="<?=$option_key?>"><?=$option['label']?></option>

            <?php endforeach; ?>

        </select>
    </div>


    <div class="hidden-specific" style="<?php if($specific_selected === true) {echo 'display: block;"';} else {echo 'display: none;"';} ?>">

        <label for="th_notification_settings_specific" style="padding-bottom: 10px;">These specific events...</label>
        <select name="th_notification_settings_specific[]" id="th_notification_settings_specific" multiple>
            <?php $checkbox_options = isset($checkbox_options) ? $checkbox_options : [];
            foreach($checkbox_options as $option_key => $option): ?>

                <option <?php if($option['selected'] === true){echo ' selected="selected" ';}?> value="<?=$option_key?>"><?=$option['label']?></option>

            <?php endforeach; ?>
        </select>

    </div>
</div>


<script>

    const th_notifs_app = {
        init() {
            const self = this;
            self.bindActions();
        },

        bindActions() {
            const self = this;

            const $th_notification_settings = jQuery('[id="th_notification_settings"');
            $th_notification_settings.unbind();
            $th_notification_settings.on('change', function(e) {
                let value = jQuery(this).val();
                if(value === 'only_specific') {
                    jQuery('.hidden-specific').show();
                } else {
                    jQuery('.hidden-specific').hide();
                }
            });
        }
    }

    th_notifs_app.init();

</script>
