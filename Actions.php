<?php

/*
 * Ajax Endpoints
 */

class Actions
{


    public function __construct() {
        $this->register();
    }

    private function register() {
        add_action( 'personal_options_update', [$this, 'updateNotificationSettings'] );
        add_action( 'edit_user_profile_update', [$this, 'updateNotificationSettings'] );

    }

    public function updateNotificationSettings($user_id) {
        if ( empty( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], 'update-user_' . $user_id ) ) {
            return;
        }
        if ( !current_user_can( 'edit_user', $user_id ) ) {
            return false;
        }

        $ret = update_user_meta( $user_id, 'th_notification_level', $_POST['th_notification_level'] );
//        update_user_meta( $user_id, 'city', $_POST['city'] );
//        update_user_meta( $user_id, 'postalcode', $_POST['postalcode'] );
    }

}
