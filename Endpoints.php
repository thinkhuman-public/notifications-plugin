<?php

include_once(plugin_dir_path( __FILE__ ) . '/Exceptions/DuplicateTemplateException.php');

/*
 * Ajax Endpoints
 */

class Endpoints
{

    const SENDGRID_API_KEY = '';

    public function __construct()
    {
        $this->registerNoPrivActions();
        $this->registerPrivActions();
    }

    private function registerNoPrivActions()
    {
        add_action('wp_ajax_nopriv_th_email_hook', [$this, 'notificationsWebhook']);
    }

    private function registerPrivActions() {
        add_action('wp_ajax_create_template', [$this, 'upsertTemplateHandler']);
    }

    public static function getTemplates() {
        $templates = get_option('th_templates') ? json_decode(get_option('th_templates'), true) : [];
        return $templates;
    }

    public static function upsertTemplate($new_template, $update = false) {
        $templates = self::getTemplates();
        $template_slug = $new_template['slug'];
        foreach($templates as $template) {
            $old_slug = $template['slug'];
            if($old_slug === $template_slug && $update === false) {
                throw new DuplicateTemplateException();
            }
        }

        // Push the new template onto the array
        $templates[] = $new_template;

        update_option('th_templates', json_encode($templates));

        return true;
    }

    public function upsertTemplateHandler() {
        $input = $_POST;

        $is_update = $input['update'] == 'true';

        $template_name = trim($input['template_name']);
        $template_slug = strtolower(str_replace(' ', '_', $template_name));

        $new_template = [
            'name' => $input['template_name'],
            'body' => $input['template_body'],
            'slug' => $template_slug
        ];

        try {
            self::upsertTemplate($new_template, $is_update);
        } catch(DuplicateTemplateException $e) {
            // Idk, who cares
        }

        wp_redirect(wp_get_referer());
        exit;
    }


    public function notificationsWebhook($args)
    {
        global $wpdb;

        header('Content-Type: application/json');

        $json_input = file_get_contents('php://input');
        $input = json_decode($json_input, TRUE); //convert JSON into array

        $options = get_option('th_notifs_plugin_options');

        // Is api_key even configured?
        if(empty($options) || $options === false || (!empty($options) && empty($options['api_key']))) {
            $this->throwError('Unauthenticated', 401);
        }

        // If it is configured are they authenticated?
        if($options['api_key'] !== $_GET['api_key']) {
            $this->throwError('Unauthenticated', 401);
        }

        $event_level = $input['level'];

        $email_attempts = [];

        try {
            $headers = array('Content-Type: text/html; charset=UTF-8');

            $prefix = $wpdb->prefix;

            $users = $wpdb->get_results(
                "SELECT u.ID, u.user_email, um.meta_key, um.meta_value 
                FROM ".$prefix."users AS u 
                    INNER JOIN ".$prefix."usermeta AS um ON um.user_id = u.ID 
                                                AND um.meta_key IN ('th_notification_settings', 'th_notification_settings_specific')"
            );

            $flat_users = [];
            foreach ($users as $user) {
                $user_id = $user->ID;
                if (!isset($flat_users[$user_id])) {
                    $flat_users[$user_id] = [
                        'email' => $user->user_email
                    ];
                }
                $flat_users[$user_id][$user->meta_key] = $user->meta_value;
            }

            foreach($flat_users as $user_id => $user) {
                $level = $user['th_notification_settings'];
                $types = [];
                if($level === 'only_specific') {
                    $types = unserialize($user['th_notification_settings_specific']);
                }

                $send = false;
                switch($level) {
                    case 'everything':
                        $send = true;
                        break;

                    case 'critical':
                        $send = $event_level === 'critical';
                        break;

                    case 'only_specific':
                        $send = in_array($event_type, $types);
                        break;

                    case 'none':
                    default:

                        $send = false;
                        break;
                }

                if($send === true) {

                    $email_request = $this->buildEmailRequest($user['email'], [
                        'level' => $event_level
                    ]);

                    $response = self::sendEmail($user['email'], $email_request['subject'], $email_request['body']);

                    $email_attempts[] = [
                        'user_id' => $user_id,
                        'success' => $response['message'] == 'success'
                    ];
                }


            }


        } catch (Exception $e) {
            $this->throwError('Email did not send. Error has been logged.');
        }

        http_response_code(200);

        echo json_encode([
            'success' => true,
            'message' => 'Emails Sent',
            'data' => $email_attempts
        ]);
        exit;
    }

    private static function sendEmail($to, $subject, $body, $html = '') {
        $params = array(
            'to'        => $to,
            'from'      => "test@test.com",
            'fromname'  => "Test",
            'subject'   => $subject,
            'text'      => $body,
            'html'      => empty($html) ? $body : $html,
        );

        $request =  'https://api.sendgrid.com/api/mail.send.json';

        $session = curl_init($request);
        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . self::SENDGRID_API_KEY));
        curl_setopt ($session, CURLOPT_POST, true);
        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($session);
        curl_close($session);

        return json_decode($response, true);
    }

    private function throwError($message, $code = 500) {
        http_response_code($code);
        echo json_encode([
            'success' => false,
            'message' => $message
        ]);
        exit;
    }

    private function buildEmailRequest($email, $event) {

        $request = [
            'to' => $email,
            'body' => 'Click here for more information on this alert : https://wp.test.com'
        ];

        $event_level_str = '';
        if($event['level'] == 'critical') {
            $event_level_str = '['.strtoupper($event['level']).'] - ';
        }

        $request['subject'] = $event_level_str . ucwords(str_replace('_', ' ', $event['type']));

        return $request;

    }

}
