<?php

/*
 * Manage plugin settings. Register options and
 */
class Settings
{
    /*
     * initiate admin settings
     */
    public function __construct()
    {
        add_action('admin_init', [$this, 'registerSettings']);
    }

    /*
     * Add menu, register wp_options settings
     */
    public function registerSettings()
    {
        // Add side menu link
        add_action('admin_menu', [$this, 'addMenu'], 11);

        // Register a new option for settings page.
        register_setting('th', 'th_options');

        // Register a new section in the settings page.
        add_settings_section(
            'th_section_managers',
            __('Section Name', 'th'),
            [$this, 'showSettingsSection'],
            'th'
        );
        // Add option fields
        add_settings_field(
            'th_option_field',
            __('New Option', 'th'),
            [$this, 'showOptionSettings'],
            'th',
            'th_section_managers',
            array(
                'label_for' => 'th_option_field',
                'class' => 'row'
            )
        );
    }

    /*
     *
     */
    public function showSettingsSection($args)
    {
        if (!current_user_can('manage_options')) {
            return;
        }
        ?>
        <p id="<?php echo esc_attr($args['id']); ?>">
            <?php esc_html_e('Option description.', 'th'); ?></p>
        <?php
    }

    /*
     * Show fields for setting options.
     */
    public function showOptionSettings($args)
    {
        if (!current_user_can('manage_options')) {
            return;
        }
        require_once('templates/settings/options.php');
    }

    /*
     * Add links to the wordpress admin side menu.
     * @link https://developer.wordpress.org/reference/functions/add_menu_page/
     */
    public function addMenu()
    {
        // This adds the menu link to the plugin sub-menu. Make sure parent menu item is added in AdminPage.php
        add_submenu_page(
            'options-general.php',
            'Settings',
            'Settings',
            'manage_options',
            'th-settings',
            [$this, 'pageSetup']
        );
    }

    /*
     * Display custom admin page from admin menu.
     */
    public static function pageSetup()
    {
        if (!current_user_can('manage_options')) {
            return;
        }
        require_once('templates/settings/settings.php');
    }
}