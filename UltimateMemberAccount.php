<?php

class UltimateMemberAccount
{
    public $version = "1.0.0.0";

    /**
     * Init Ultimate Member Hooks
     */
    public function __construct()
    {
        add_filter('um_account_page_default_tabs_hook', [$this, 'um_account_custom_tab_th']);
        add_filter('um_account_content_hook_th_notifications', [$this, 'um_account_custom_tab_content_th'], 20, 2);
        add_action('um_after_user_account_updated', [$this, 'um_account_custom_tab_submit_th'], 20, 2);

        // Add validation later when we need it
        // add_action( 'um_submit_account_errors_hook', [$this, 'um_account_custom_tab_errors_th'], 20 );
    }

    /**
     * @param $tabs
     *
     * Attach tab with a bell to the account page
     *
     * @return mixed
     */
    public function um_account_custom_tab_th($tabs)
    {
        $tabs[109] = [];
        $tabs[109]['th_notifications'] = [
            'icon' => 'um-faicon-bell',
            'title' => __('Notification Settings', 'th-notification'),
            'submit_title' => __('Save', 'th-notification'),
            'custom' => true,
        ];
        return $tabs;
    }

    /**
     * @param $output
     * @param $args
     *
     * Render templates/notification_settings.php template into the Custom Account Tab
     *
     * @return mixed
     */
    public function um_account_custom_tab_content_th($output = '', $args = array())
    {
        global $current_user;

        $notification_settings = get_user_meta($current_user->ID, 'th_notification_settings');
        $specific_settings = get_user_meta($current_user->ID, 'th_notification_settings_specific');

        // Flatten this structure, idk why it's an array
        if (!empty($notification_settings) && is_array($notification_settings)) {
            $notification_settings = $notification_settings[0];
        }

        // Even when it's an array, it's an array of arrays, weeeeeee
        if (!empty($specific_settings) && is_array($specific_settings)) {
            $specific_settings = $specific_settings[0];
        }


        // Initialize inputs to be rendered in notification_settings.php form
        $settings_options = [
            'everything' => [
                'label' => 'Everything',
                'selected' => false,
            ],

            'critical' => [
                'label' => 'Critical',
                'selected' => false,
            ],

            'only_specific' => [
                'label' => 'Only Specific',
                'selected' => false,
            ],

            'none' => [
                'label' => 'None',
                'selected' => false,
            ],
        ];
        $checkbox_options = [
            'battery' => [
                'label' => 'Battery',
                'selected' => false,
            ],

            'water_level' => [
                'label' => 'Water Level',
                'selected' => false,
            ],

            'lost' => [
                'label' => 'Lost',
                'selected' => false,
            ],

            'tip_over' => [
                'label' => 'Tip Over',
                'selected' => false,
            ],

            'wifi_connection' => [
                'label' => 'Wifi Connection',
                'selected' => false,
            ]
        ];

        // Add a selected flag if they have already selected their notification settings
        if (!empty($notification_settings) && isset($settings_options[$notification_settings])) {
            $settings_options[$notification_settings]['selected'] = true;
        }

        foreach ($checkbox_options as $key => $option) {
            if (in_array($key, $specific_settings)) {
                $checkbox_options[$key]['selected'] = true;
            }
        }

        // Start the output buffer so we can capture the template and return it
        // upstream to the ultimate member boilerplate
        ob_start();

        include('templates/notification_settings.php');

        $output .= ob_get_clean(); // We just add the above template to the output

        return do_shortcode($output);
    }

    /**
     * @param $user_id
     * @param $changes
     *
     * Persist the custom fields updates to usermeta database
     *
     * @return void
     */
    public function um_account_custom_tab_submit_th($user_id, $changes)
    {
        if (isset($_POST['th_notification_settings']) && !UM()->form()->has_error('th_notification_settings')) {
            update_user_meta($user_id, 'th_notification_settings', $_POST['th_notification_settings']);

            // If they have selected anything other than only_specific then clear specific settings to prevent
            // dirty data
            if ($_POST['th_notification_settings'] != 'only_specific') {
                $_POST['th_notification_settings_specific'] = [];
            }

        }

        if (isset($_POST['th_notification_settings_specific']) && !UM()->form()->has_error('th_notification_settings_specific')) {
            update_user_meta($user_id, 'th_notification_settings_specific', $_POST['th_notification_settings_specific']);
        }
    }


    /**
     * NOT IN USE
     * Validate custom fields example
     *
     * @param array $post_args
     * @return void
     */
    public function um_account_custom_tab_errors_th($post_args)
    {

//        if (!isset($post_args['um_account_submit'])) {
//            return;
//        }
//
//        if (isset($post_args['wc_custom_01']) && (strlen($post_args['wc_custom_01']) < 3 || 99 < strlen($post_args['wc_custom_01']))) {
//            UM()->form()->add_error('wc_custom_01', __('This field length should be between 3 and 99', 'ultimate-member'));
//        }
    }
}